module ApplicationHelper
    #check the url and append active class to it
    def is_active?(link_path)
     current_page?(link_path) ? "active" : ""
    end
end
